package com.sample.sink1;

import java.time.LocalDateTime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;

@SpringBootApplication
@EnableBinding(Sink.class)
@EnableSchemaRegistryClient
public class SinkV1Application {

	
	public static void main(String[] args) {
		SpringApplication.run(SinkV1Application.class, args);
	}
	
	
	
	@StreamListener(Sink.INPUT)
	public void println(Book book) {
		System.out.printf (
                "[%s] received book, isbn = %s, title = %s, price = %d, version = %s %n",
                LocalDateTime.now(),
                book.getIsbn(),
                book.getTitle(),
                book.getPrice(),
                book.getVersion()
        );
		
	}

}
